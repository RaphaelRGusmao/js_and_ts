const test = require('ava');

function scaffoldStructure(document, data) {
  const ul = document.createElement('ul');

  data.forEach(element => {
    const li = document.createElement('li');

    const name = document.createElement('b');
    name.innerText = element.name;
    name.className = 'name';
    li.append(name);

    const email = document.createElement('span');
    email.innerText = ' - ' + element.email;
    li.append(email);

    ul.append(li);
  })

  document.body.appendChild(ul);
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  // Test 1
  const ul_list = document.getElementsByTagName('ul');
  t.is(ul_list.length, 1);

  // Test 2
  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);

  // Test 3
  let names = [];
  nameNodes.forEach(node => names.push(node.innerText));
  let sorted_names = names.sort();
  t.is(names, sorted_names);

  // Test 4
  const li_list = document.querySelectorAll('li');
  li_list.forEach(li => {
    const name = li.querySelector('.name');
    t.is(name.tagName, "B");
  });
});
