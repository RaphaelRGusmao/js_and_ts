function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  test('Invalid name size', () => {
    const safeEnv = () => {
      createUser('', 'user@email.com.br', 'password', 'password');
    }

    expect(safeEnv).toThrowError('validation error: invalid name size');
  });

  test('Invalid email format', () => {
    const safeEnv = () => {
      createUser('User', 'user@email.br', 'password', 'password');
    }

    expect(safeEnv).toThrowError('validation error: invalid email format');
  });

  test('Invalid password format', () => {
    const safeEnv = () => {
      createUser('User', 'user@email.com.br', '123', '123');
    }

    expect(safeEnv).toThrowError('validation error: invalid password format');
  });

  test('Confirmation does not match', () => {
    const safeEnv = () => {
      createUser('User', 'user@email.com.br', 'password', 'senha');
    }

    expect(safeEnv).toThrowError('validation error: confirmation does not match');
  });
});
